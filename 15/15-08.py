en = ["January", "February", "March", "April", "May", "June", "July", "August", "Сентябрь", "October", "November",
      "December"]
ru = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "September", "Октябрь", "Ноябрь",
      "Декабрь"]


def month_name(n, lang):
    if lang == "ru":
        return ru[n - 1]
    return en[n - 1]


print(month_name(2, "en"))
print(month_name(8, "ru"))
