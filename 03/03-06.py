first = input()
second = input()
while len(first) < 8 or "123" in first or first != second:
    if len(first) < 8:
        print("Пароль короткий")
        first = input()
        second = input()
    elif "123" in first:
        print("Пароль простой")
        first = input()
        second = input()
    elif first != second:
        print("Различаются")
        first = input()
        second = input()
else:
    print("ok")
