print(' '.join([str(int(num) ** 2) for num in input().split() if (int(num) ** 2) % 10 != 9 and int(num) % 2 != 0]))
