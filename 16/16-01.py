def circle_length(radius):
    return 2 * 3.14 * radius


def circle_area(radius):
    return 3.14 * (radius ** 2)


def main():
    rad = float(input())
    print(circle_length(rad))
    rad = float(input())
    print(circle_area(rad))


main()
