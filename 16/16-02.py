def discriminant(a, b, c,):
    return (b ** 2) - 4 * a * c


def larger_root(p, q):
    return (-p + discriminant(1, p, q)) // 2


def smaller_root(p, q):
    return (-p - discriminant(1, p, q)) // 2


def main():
    print(smaller_root(2, 1))
    print(larger_root(2, 1))


main()
