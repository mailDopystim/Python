base = ["Иван", "Юлия Иванкова"]


def hello(name):
    global names
    names = name
    print("Здравствуйте,", names, " Вашу карту ищут...")


def search_card(name):
    global base
    if names in base:
        print("Ваша карта с номером ", base.index(names) + 1, "найдена")
    else:
        print("Ваша карта не найдена")


hello("Иван")
search_card("Иван")
hello("Юлия Иванова")
search_card("Юлия Иванова")
