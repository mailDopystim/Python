def setup_profile(name, vacation_dates):
    global names
    names = name
    global dt
    dt = vacation_dates


def print_application_for_leave():
    print('Заявление на отпуск в период', dt + '.', names)


def print_holiday_money_claim(amount):
    print('Прошу выплатить', amount, 'отпускных денег', names)


def print_attorney_letter(to_whom):
    print('На время отпуска в период', dt, 'моим заместителем назначается',
          to_whom + '.', names)


setup_profile("Иван Петров", "1 июня – 20 июня")
print_application_for_leave()
print_application_for_leave()
print_holiday_money_claim("15 тысяч пиастров")
print_attorney_letter("Василий Васильев")
