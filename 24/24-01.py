from PIL import Image, ImageDraw


def twist_image(input_file_name, output_file_name):
    im = Image.open(input_file_name)
    x, y = im.size
    left = im.crop((0, 0, int(0.5 * x), y))
    # res_left = left.transpose(Image.FLIP_LEFT_RIGHT).transpose(Image.ROTATE_180)
    right = im.crop((int(0.5 * x), 0, x, y))
    # res_right = right.transpose(Image.FLIP_LEFT_RIGHT).transpose(Image.ROTATE_180)
    im.paste(right, (0, 0))
    im.paste(left, (int(x * 0.5), 0))
    im.save(output_file_name)


twist_image("statement-image.jpg", "crop_statement-image.jpg")
