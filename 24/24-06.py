from PIL import Image


def make_preview(size, n_colors):
    im = Image.open('image.jpg')
    re = im.resize(size)
    res = re.quantize(n_colors)
    res.save("re and qua.bmp")


make_preview((400, 200), 64)
