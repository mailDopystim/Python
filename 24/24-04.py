from PIL import Image, ImageFilter


def motion_blur(n):
    im = Image.open("image.jpg")
    mov = im.transpose(Image.ROTATE_270)
    gauss = mov.filter(ImageFilter.GaussianBlur(radius=n))
    gauss.save("res im.jpg")


motion_blur(10)
