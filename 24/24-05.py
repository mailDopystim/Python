import wave
import struct


source = wave.open("in.wav", mode="rb")
dest = wave.open("out.wav", mode="wb")

dest.setparams(source.getparams())
frames_count = source.getnframes()
data = struct.unpack("<" + str(frames_count) + "h", source.readframes(frames_count))
list_data = list(data)
# print(list_data)
new_data = list(filter(lambda x: -5 >= x <= 5, list_data))
not_silence = tuple(new_data)
newframes = struct.pack("<" + str(len(not_silence)) + "h", *not_silence)

dest.writeframes(newframes)
source.close()
dest.close()
