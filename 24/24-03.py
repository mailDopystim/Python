import wave
import struct


def chip_and_dale(number):
    source = wave.open("in.wav", mode="rb")
    dest = wave.open("out.wav", mode="wb")

    dest.setparams(source.getparams())
    frames_count = source.getnframes()
    data = struct.unpack("<" + str(frames_count) + "h", source.readframes(frames_count))

    new_data = data[::number]

    newframes = struct.pack("<" + str(len(new_data)) + "h", *new_data)

    dest.writeframes(newframes)
    source.close()
    dest.close()


chip_and_dale(2)
