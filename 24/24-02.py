from PIL import Image


def transparency(filename1, filename2):
    fish = Image.open(filename1)
    space = Image.open(filename2)
    pixel1 = fish.load()
    pixel2 = space.load()

    x, y = fish.size
    for i in range(x):
        for j in range(y):
            r1, g1, b1 = pixel1[i, j]
            r2, g2, b2 = pixel1[i, j]
            r = int(0.5 * r1 + 0.5 * r2)

    fish.save('res fish space.jpg')


transparency("fish.jpg", "space.jpg")
#####