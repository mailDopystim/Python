def partial_sums(n, *m):
    res = n
    _list = [0, res]
    for i in m:
        res += i
        _list.append(res)
    return _list


print(partial_sums(13))
print(partial_sums(1, 0.5, 0.25, 0.125))
