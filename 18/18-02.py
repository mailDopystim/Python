def message(name, date, email, place="Neftik"):
    print("To:", email)
    print("Здравствуйте,", name, "!")
    print("Были бы рады видеть вас на встрече начинающих программистов в ", place, ", которая пройдет ", date, ".")


message("Ivan", "3.01.2021", "ivan@gmaii.com", "Moscow")
message("Ivan2", "27.12.2020", "ivan2@gmaii.com", "LA")
message("Ivan3", "00.12.2020", "ivan3@gmaii.com")
