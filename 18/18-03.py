def encrypt_caesar(text, shift=1001):
    text = list(text)
    for i in range(len(text)):
        char = text[i]
        c = ord(char)
        if char.isalpha():
            if char.islower():
                _min, _max = ord('а'), ord('я')
            else:
                _min, _max = ord('А'), ord('Я')
            if _min <= c <= _max:
                c += shift
                while c > _max:
                    c = c - _max + _min
        text[i] = chr(c)
    return ''.join(text)


def decrypt_caesar(text, shift=1001):
    text = list(text)
    for i in range(len(text)):
        char = text[i]
        c = ord(char)
        if char.isalpha():
            if char.islower():
                _min, _max = ord('а'), ord('я')
            else:
                _min, _max = ord('А'), ord('Я')
            if _min <= c <= _max:
                c -= shift
                while c < _min:
                    c = c + _max - _min
        text[i] = chr(c)
    return ''.join(text)


msg = "Да здравствует салат Цезарь!"
shift = 3
encrypted = encrypt_caesar(msg, shift)
decrypted = decrypt_caesar(encrypted, shift)
print(encrypted)
print(decrypted)
