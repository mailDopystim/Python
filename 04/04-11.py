n = int(input())
s = 0
c = 0
for i in range(1, n + 1):
    a = int(input())
    s += a
    c = s // i
    if i == 1:
        print(0)
    elif a > c:
        print(">")
    elif a < c:
        print("<")
