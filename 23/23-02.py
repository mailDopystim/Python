from PIL import Image

im = Image.open("image.jpg")
pixels = im.load()  # список с пикселями
x, y = im.size  # ширина (x) и высота (y) изображения
x1, y1, z1 = 0, 0, 0

for i in range(x):
    for j in range(y):
        r, g, b = pixels[i, j]
        x1 += r
        y1 += g
        z1 += b

x1 = x1 // (x * y)
y1 = y1 // (x * y)
z1 = z1 // (x * y)

print(x1, y1, z1)
