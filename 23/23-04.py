from PIL import Image, ImageDraw


new_image = Image.new("RGB", (512, 200), (0, 0, 0))
pixels = new_image.load()  # список с пикселями
draw = ImageDraw.Draw(new_image)
for i in range(0, 512, 2):
    draw.line((i, 0, i, 200), fill=(i // 2, 0 , 0), width=2)


new_image.save('res.png')




