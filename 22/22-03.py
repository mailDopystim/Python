from random import choices


def generate_password(m):
    sym = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789'
    return ''.join(choices(sym, k=m))


def main(n, m):
    res = []
    for i in range(n):
        res.append(generate_password(m))
    return res


print("Случайный пароль из 7 символов:", generate_password(7))
print("10 случайных паролей длиной 15 символов:")
print(*main(10, 15), sep="\n")

