import pymorphy2


morph = pymorphy2.MorphAnalyzer()

for i in range(100, 0, -1):
    bottle = morph.parse('бутылка')[0]
    print("В холодильнике ", i, bottle.make_agree_with_number(i).word, " кваса.\n")
    print("Возьмём одну и выпьем.\n")
    print("Осталось ", i - 1, bottle.make_agree_with_number(i - 1).word, "кваса.")
