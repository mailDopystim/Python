import pymorphy2
morph = pymorphy2.MorphAnalyzer()

cas = {"именительный" : "nomn",
        "родительный" :  "gent",
        "дательный" : "datv",
        "винительный" : "accs",
        "творительный" : "ablt",
        "предложный" : "loct"}
word = morph.parse(input())[0]
if "NOUN" in word.tag.POS:
    print("Единственное число:")
    for i in cas:
        print(i, word.inflect({"sing", cas[i]}).word)
    print("Множественное число:")
    for i in cas:
        print(i, word.inflect({"plur", cas[i]}).word)
else:
    print("ne sychestvitelinoe")
