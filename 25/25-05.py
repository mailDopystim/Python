import pymorphy2
morph = pymorphy2.MorphAnalyzer()

gndr = {
        "masc": "мужской",
        "femn" : "женский род",
        "neut" : "средний род"
}
pers = {
    "1per" : "1 лицо",
    "2per" : "2 лицо",
    "3per" : "3 лицо"
}
word = morph.parse(input())[0]
if "INFN" in word.tag.POS:
    print("Прошедшее время:")
    for i in gndr:
        print(word.inflect({"past", i}).word)
    print(word.inflect({"plur", "past"}).word)
    print("Настоящее время:")
    for i in pers:
        print(word.inflect({"pres", "sing", i}).word)
        print(word.inflect({"pres", "plur", i}).word)
else:
    print("ne glagol")
