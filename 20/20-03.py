import sys


_sum = 0
count = 0
for i in sys.stdin:
    _sum += int(i)
    count += 1
if _sum:
    print(_sum / count)
else:
    print(-1)
