import sys

n = list(map(lambda x: x.lstrip().startswith("#"), sys.stdin))
print(n.count(True))
