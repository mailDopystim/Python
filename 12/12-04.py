n = ([int(i) for i in input().split()])
M, K = input().split()
n = n[int(M):int(K) + 1]
s = 0
for i in n:
    s += (i ** 2)
print(s)
