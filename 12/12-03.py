n = ([int(i) for i in input().split()])
M, K = input().split()
print(sum(n[int(M):int(K) + 1]))
