n = input().split()
stack = []
res = 0
for i in n:
    if i == "+":
        res = int(stack.pop()) + int(stack.pop())
        stack.append(res)
    elif i == "-":
        res = - int(stack.pop()) + int(stack.pop())
        stack.append(res)
    elif i == "*":
        res = int(stack.pop()) * int(stack.pop())
        stack.append(res)
    else:
        stack.append(i)
print(stack[0])
