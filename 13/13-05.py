n = int(input())
phone = dict()
for i in range(n):
    val, key = input().split()
    if key not in phone:
        phone[key] = val
m = int(input())
for i in range(m):
    name = input()
    if name in phone:
        print(phone[name])
    else:
        print("NO")
