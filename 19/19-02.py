def simple_map(transformation, values):
    tr_values = []
    for i in values:
        tr_values.append(transformation(i))
    return tr_values


values = [1, 3, 1, 5, 7]
print(*simple_map(lambda x: x + 5, values))
