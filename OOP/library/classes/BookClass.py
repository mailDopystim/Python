class Book:
    bookNum = 1

    def __init__(self, name, author, year):
        self.available = True
        self.num = Book.bookNum
        Book.bookNum += 1
        self.name = name.strip()
        self.author = author.strip()
        self.year = year.strip()

    def change_book(self, newNum="", newName="", newAuthor="", newYear=""):
        if not self.available:
            print("Такой книги нет")
            return
        if newNum != "":
            self.num = int(str(newNum).strip())
        if newName != "":
            self.name = newName.strip()
        if newAuthor != "":
            self.author = newAuthor.strip()
        if newYear != "":
            self.year = newYear.strip()

    def __del__(self):
        self.available = False
        self.name = ""
        self.year = 0
        self.author = ""
        self.num = 0

    def show(self):
        print(f"{self.num} | {self.name} | {self.author} | {self.year}")
        print("----------------------")
