from classes.BookClass import Book

class Library:
    def __init__(self):
        self.bookList = list()

    def open(self):
        file = open('list.txt', encoding='utf-8')
        for line in file:
            self.bookList.append(Book(line.split("|")[0], line.split("|")[1], line.split("|")[2]))

    def add_book(self):
        print("Введите название:", end=" ")
        n = input()
        print("----------------------")
        print("Введите автора:", end=" ")
        a = input()
        print("----------------------")
        print("Введите год:", end=" ")
        y = input()
        print("----------------------")
        self.bookList.append(Book(n, a, y))
        self.close()

    # def find_book(self, find):
        # findList = list(filter(lambda x: find in x, self.bookList))
        # for i in findList:
        #     if i.name == find:
        #         x = i
        #         return x
        # print("Такой книги нет в нашем списке")
        # print("----------------------")
        # return 0

        # for i in self.bookList:
        #     if i.name == find:
        #         x = i
        #         return x
        # print("Такой книги нет в нашем списке")
        # print("----------------------")
        # return 0

    def find_books(self, f):
        listReturn = list()
        for i in self.bookList:
            if f.upper().strip() in i.name.upper():
                listReturn.append(i)
        if len(listReturn) != 0:
            return listReturn
        return 0

    def print_book(self):
        print("Введитее название:", end=" ")
        find = input()
        print("----------------------")
        k = self.find_books(find)
        if k != 0:
            for i in k:
                i.show()


    def change_book(self):
        print("Введите текущее название:", end=" ")
        f = input()
        print("----------------------")
        s = self.find_books(f)
        if s == 0:
            print("Книги нет")
            return
        print("Выберите книгу")
        t = 1
        for l in s:
            print(str(t) + "|" + str(l.name))
            t += 1
        j = int(input("Введите номер: "))
        if j > t:
            print("Не правильная команда")
            return
        print("Введите новое название:" + "(" + s[j - 1].name + ")", end=" ")
        n = input()
        print("----------------------")
        print("Введите нового автора:" + "(" + s[j - 1].author + ")", end=" ")
        a = input()
        print("----------------------")
        print("Введите новый год:" + "(" + s[j - 1].year + ")", end=" ")
        y = input()
        print("----------------------")
        s[j - 1].change_book(s[j - 1].num, n, a, y)
        self.close()


    def delete_book(self):
        print("Введите название:", end=" ")
        name = input()
        print("----------------------")
        i = self.find_books(name)
        if i == 0:
            print("книги нет")
            return
        print("Выберите книгу")
        t = 1
        for l in i:
            print(str(t) + "." + str(l.name))
            t += 1
        j = int(input("Введите номер: "))
        if j > t:
            print("Не правильная команда")
            return
        self.bookList.remove(i[j-1])
        i[j-1].__del__()
        Book.bookNum = 1
        for z in self.bookList:
            z.change_book(Book.bookNum, z.name, z.author, z.year)
            Book.bookNum += 1
        self.close()


    def get_all_books(self):
        if len(self.bookList) == 0:
            print("Книг нет")
            return
        for i in self.bookList:
            i.show()


    def close(self):
        file = open('list.txt', 'w', encoding='utf-8')
        for i in self.bookList:
            file.write(i.name + "|" + i.author + "|" + i.year + "\n")


