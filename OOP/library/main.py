from classes.LibraryClass import Library
myLibrary = Library()
myLibrary.open()
while True:
    print("--------------------------")
    print("1| Добавить книгу")
    print("-+------------------------")
    print("2| Найти книгу (по названию)")
    print("-+------------------------")
    print("3| Изменить книгу  ")
    print("-+------------------------")
    print("4| Удалить книгу ")
    print("-+------------------------")
    print("5|Вывести все книги")
    print("-+------------------------")
    print("6| Выход")
    print("--------------------------")
    t = input("Введите команду: ")
    print("----------------------")
    print("")
    if t == "1":
        myLibrary.add_book()
        print("")
    elif t == "2":
        myLibrary.print_book()
        print("")
    elif t == "3":
        myLibrary.change_book()
        print("")
    elif t == "4":
        myLibrary.delete_book()
        print("")
    elif t == "5":
        myLibrary.get_all_books()
        print("")
    elif t == "6":
        myLibrary.close()
        break
    else:
        print("Вы ввели не правильную команду")
        print("")

