first = set()
second = set()
flag = True
while True:
    num = input()
    if num != "":
        if flag:
            first.add(num)
        else:
            second.add(num)
    else:
        if flag:
            flag = False
        else:
            break
result = first & second
if len(result) == 0:
    print("Empty")
else:
    for num in result:
        print(result)
