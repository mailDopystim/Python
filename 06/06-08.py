N = int(input())
surnames = set()
surnames2 = set()
count = 0
for i in range(N):
    surname = input()
    if surname in surnames:
        surnames2.add(surname)
        count += 1
    else:
        surnames.add(surname)
print(count + len(surnames2))
