m = int(input())
n = int(input())
en = set()
de = set()
for i in range(m):
    de.add(input())
for i in range(n):
    en.add(input())
intersection = en & de
if len(en) + len(de) - (len(intersection) * 2) == 0:
    print("no")
else:
    print(len(en) + len(de) - (len(intersection) * 2))
