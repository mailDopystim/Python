m = int(input())
n = int(input())
en_de = set()
sur = set()
for i in range(m + n):
    surname = input()
    if surname in en_de:
        sur.add(surname)
    else:
        en_de.add(surname)
if (en_de - sur) == 0:
    print("no")
else:
    print(len(en_de - sur))
