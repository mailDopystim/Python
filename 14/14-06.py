def print_average(arr):
    if not arr:
        print(0)
    else:
        print(sum(arr)/len(arr))


# print_average([])
# print_average([79])
